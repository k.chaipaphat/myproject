import api from './../api';
import apiFormData from './../api/formData';


export const getDataUser =  ({commit},info) => { 
    var data  = {
        data: info,
        url:'api/users',
        text_commit:"SET_USER"
    }
     return api({commit}, data) 
};