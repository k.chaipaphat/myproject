import axios from 'axios'
//import Vue from 'vue'

export default async function ({commit},data) { 
    if(data){
      //  var token = Vue.cookie.get("token")
        const response = await axios({
                    method: "get",
                    url: `http://localhost/${data.url}`,
             //       headers: {"Authorization": token},
                    data: data.data
                })
        if(data.text_commit){
            commit(data.text_commit, response.data);
        }
        return response.data
    }
    
}