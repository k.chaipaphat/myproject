require('./bootstrap');

window.Vue = require('vue');
window.axios=require('axios');

import { store } from './vuex'

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('user-component', require('./components/User.vue').default);
Vue.component('create-component', require('./components/Create.vue').default);

const app = new Vue({
    el: '#app',
    store
});


