<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mob Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
      <div class="flex-center position-ref full-height">
          <div class="content">
            <div id="app">
                <user-component></user-component>
            </div>
          </div>
      </div>
    </body>
    <script src="js/app.js" charset="utf-8"></script>
</html>
